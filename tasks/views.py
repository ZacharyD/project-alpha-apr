from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
        context = {
            "form": form,
        }
    return render(request, "create/create_task.html", context)


@login_required
def list_tasks(request):
    tasks = Task.objects.filter()
    context = {
        "tasks": tasks,
    }
    return render(request, "create/list_tasks.html", context)
